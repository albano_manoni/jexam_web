'use strict';

/**
 * @ngdoc overview
 * @name jexamApp
 * @description
 * # jexamApp
 *
 * Main module of the application.
 */
angular
  .module('jexamApp', [
    'ngCookies',
    'ngRoute',
    'ngSanitize',
    'restangular'
  ])
  .config(function ($routeProvider) {
    $routeProvider
      .when('/', {
        templateUrl: 'views/main.html',
        controller: 'MainCtrl',
        controllerAs: 'main'
      })
      .when('/login', {
        templateUrl: 'views/login.html',
        controller: 'LoginCtrl',
        controllerAs: 'login'
      })
      .otherwise({
        redirectTo: '/'
      });
  }).config(function (RestangularProvider, Config) {
  RestangularProvider.setBaseUrl(Config.apiBaseUrl);
  RestangularProvider.setRequestSuffix("/");
  RestangularProvider.setExtraFields(['name']);
  RestangularProvider.setDefaultRequestParams('get', {format: 'json', 't': new Date().getTime()});
  RestangularProvider.setResponseExtractor(function (response, operation) {
    return response.objects;
  });
}).run(function ($rootScope, $location, UserData) {
  $rootScope.$on("$routeChangeStart", function (event, next, current) {
    if (sessionStorage.loggedUser !== null && sessionStorage.loggedUser !== undefined && sessionStorage.loggedUser !== "") {
      var loggedUser = JSON.parse(sessionStorage.loggedUser);
      var token = loggedUser.token;
      if (token == null || token == undefined || token === "") {
        if (next.templateUrl == "views/login.html") {
        } else {
          $location.path("/login");
        }
      } else {
        UserData.init(loggedUser);
      }
    } else {
      $location.path("/login");
    }

  });
});
