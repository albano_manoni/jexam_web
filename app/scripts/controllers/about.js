'use strict';

/**
 * @ngdoc function
 * @name jexamApp.controller:AboutCtrl
 * @description
 * # AboutCtrl
 * Controller of the jexamApp
 */
angular.module('jexamApp')
  .controller('AboutCtrl', function () {
    this.awesomeThings = [
      'HTML5 Boilerplate',
      'AngularJS',
      'Karma'
    ];
  });
