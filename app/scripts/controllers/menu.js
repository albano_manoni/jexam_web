'use strict';

/**
 * @ngdoc function
 * @name jexamApp.controller:MenuCtrl
 * @description
 * # MenuCtrl
 * Controller of the jexamApp
 */
angular.module('jexamApp')
  .controller('MenuCtrl', function ($scope, $location, Login, UserData) {
    $scope.$watch(function () {
      return UserData.username;
    }, function () {
      $scope.username = UserData.username;
    });

    $scope.logout = function () {
      Login.logout();
    };

    $scope.isActive = function (viewLocation) {
      return viewLocation === $location.path();
    };

  });
