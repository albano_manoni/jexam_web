'use strict';

/**
 * @ngdoc function
 * @name jexamApp.controller:MainCtrl
 * @description
 * # MainCtrl
 * Controller of the jexamApp
 */
angular.module('jexamApp')
  .controller('MainCtrl', function () {
    this.awesomeThings = [
      'HTML5 Boilerplate',
      'AngularJS',
      'Karma'
    ];
  });
