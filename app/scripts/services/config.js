'use strict';

/**
 * @ngdoc service
 * @name jexamApp.Config
 * @description
 * # Config
 * Constant in the jexamApp.
 */
angular.module('jexamApp')
  .constant('Config', {
    clientId: 'EtdChoXlPvuZCr3bY8bOdA3RNGIUSUiFW3C3lGvP',
    clientSecret: 'AKbng2iUDAF8W62yYPz4DnQEjGvJzotbip5iNA3TEvnEpVu10vfLM6hqeoYjM0nFgQQFwGWaIFBllKhNqJv9W1RDJYy6HxpKkWIofG8RujleYUu5FGCJeIFZSE8YvFiX',
    apiBaseUrl: 'http://jexam.herokuapp.com/api/v1/',
    tokenUrl: 'http://jexam.herokuapp.com/o/token/'
  });
