'use strict';

/**
 * @ngdoc service
 * @name jexamApp.Resource
 * @description
 * # Resource
 * Factory in the jexamApp.
 */
angular.module('jexamApp')
  .factory('Resource', function (Restangular, Util) {
    var user = Restangular.all('user');
    var assessments = Restangular.all('assessment');
    return {
      getUser: function () {
        return user.getList(Util.getAuthorizationParameter());
      },
      getAssessments: function () {
        return assessments.getList(Util.getAuthorizationParameter());
      }
    };
  });
