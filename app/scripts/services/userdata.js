'use strict';

/**
 * @ngdoc service
 * @name jexamApp.UserData
 * @description
 * # UserData
 * Service in the jexamApp.
 */
angular.module('jexamApp')
  .service('UserData', function () {

    this.init = function (loggedUser) {
      this.username = loggedUser.username;
      this.email = loggedUser.email;
      this.token = loggedUser.token;
    };

    this.reset = function () {
      this.username = "";
      this.email = "";
      this.token = "";
    }
  });
